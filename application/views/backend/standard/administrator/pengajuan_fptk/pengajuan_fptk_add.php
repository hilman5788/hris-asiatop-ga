
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pengajuan Fptk        <small><?= cclang('new', ['Pengajuan Fptk']); ?> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/pengajuan_fptk'); ?>">Pengajuan Fptk</a></li>
        <li class="active"><?= cclang('new'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Pengajuan Fptk</h3>
                            <h5 class="widget-user-desc"><?= cclang('new', ['Pengajuan Fptk']); ?></h5>
                            <hr>
                        </div>
                        <?= form_open('', [
                            'name'    => 'form_pengajuan_fptk', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_pengajuan_fptk', 
                            'enctype' => 'multipart/form-data', 
                            'method'  => 'POST'
                            ]); ?>
                         
                         
                                                <div class="form-group ">
                            <label for="jabatan" class="col-sm-3 control-label">Jabatan 
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="jabatan" id="jabatan" placeholder="Jabatan" value="<?= set_value('jabatan'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="jumlah" class="col-sm-3 control-label">Jumlah 
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="jumlah" id="jumlah" placeholder="Jumlah" value="<?= set_value('jumlah'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="status_dibutuhkan" class="col-sm-3 control-label">Status Dibutuhkan 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="status_dibutuhkan" id="status_dibutuhkan" data-placeholder="Status Dibutuhkan" >
                                    <option value=""></option>
                                    <option value="Urgent">Urgent</option>
                                    <option value="Normal">Normal</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="atasan_langsung" class="col-sm-3 control-label">Atasan Langsung 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="atasan_langsung" id="atasan_langsung" data-placeholder="Atasan Langsung" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('aauth_users') as $row): ?>
                                    <option value="<?= $row->id ?>"><?= $row->full_name; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="alasan_permohonan" class="col-sm-3 control-label">Alasan Permohonan 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="alasan_permohonan" id="alasan_permohonan" data-placeholder="Alasan Permohonan" >
                                    <option value=""></option>
                                    <option value="Penambahan MPP">Penambahan MPP</option>
                                    <option value="Penggantian Karyawan Resign">Penggantian Karyawan Resign</option>
                                    <option value="Penggantian Karyawan Resign">Penggantian Karyawan Resign</option>
                                    <option value="Pengisian Jabatan Baru">Pengisian Jabatan Baru</option>
                                    <option value="Penggantian Akibat Rotasi/Mutasi">Penggantian Akibat Rotasi/Mutasi</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="penempatan" class="col-sm-3 control-label">Penempatan 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="penempatan" id="penempatan" placeholder="Penempatan" value="<?= set_value('penempatan'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="jenis_kelamin" class="col-sm-3 control-label">Jenis Kelamin 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="jenis_kelamin" id="jenis_kelamin" data-placeholder="Jenis Kelamin" >
                                    <option value=""></option>
                                    <option value="L">Laki-laki</option>
                                    <option value="P">Perempuan</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="usia" class="col-sm-3 control-label">Usia 
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="usia" id="usia" placeholder="Usia" value="<?= set_value('usia'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="status_karyawan" class="col-sm-3 control-label">Status Karyawan 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="status_karyawan" id="status_karyawan" data-placeholder="Status Karyawan" >
                                    <option value=""></option>
                                    <option value="PKWT">PKWT</option>
                                    <option value="PWTT">PWTT</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="marital_status" class="col-sm-3 control-label">Marital Status 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="marital_status" id="marital_status" data-placeholder="Marital Status" >
                                    <option value=""></option>
                                    <option value="Single">Single</option>
                                    <option value="Married">Married</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="pendidikan" class="col-sm-3 control-label">Pendidikan 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="pendidikan" id="pendidikan" data-placeholder="Pendidikan" >
                                    <option value=""></option>
                                    <option value="SMA / STM / SMK">SMA / STM / SMK</option>
                                    <option value="Diploma 3">Diploma 3</option>
                                    <option value="S1">S1</option>
                                    <option value="S2">S2</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="pengalaman" class="col-sm-3 control-label">Pengalaman 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="pengalaman" id="pengalaman" data-placeholder="Pengalaman" >
                                    <option value=""></option>
                                    <option value="1 - 2 tahun">1 - 2 tahun</option>
                                    <option value="2 - 3 tahun">2 - 3 tahun</option>
                                    <option value="> 3 tahun">> 3 tahun</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="keterampilan" class="col-sm-3 control-label">Keterampilan 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="keterampilan[]" id="keterampilan" data-placeholder="Keterampilan" multiple >
                                    <option value=""></option>
                                    <option value="Administratif / Filling">Administratif / Filling</option>
                                    <option value="Pelayanan Pelanggan / Collection">Pelayanan Pelanggan / Collection</option>
                                    <option value="Teknisi Elektronik">Teknisi Elektronik</option>
                                    <option value="Komputer Ms Word / Excel">Komputer Ms Word / Excel</option>
                                    <option value="Pemrograman">Pemrograman</option>
                                    <option value="Lainnya">Lainnya</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                           <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
                   
      $('#btn_cancel').click(function(){
        swal({
            title: "<?= cclang('are_you_sure'); ?>",
            text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/pengajuan_fptk';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_pengajuan_fptk = $('#form_pengajuan_fptk');
        var data_post = form_pengajuan_fptk.serializeArray();
        var save_type = $(this).attr('data-stype');

        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: BASE_URL + '/administrator/pengajuan_fptk/add_save',
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            resetForm();
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
                
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
       
 
       
    
    
    }); /*end doc ready*/
</script>