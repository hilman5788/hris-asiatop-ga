
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pengajuan Training        <small><?= cclang('new', ['Pengajuan Training']); ?> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/pengajuan_training'); ?>">Pengajuan Training</a></li>
        <li class="active"><?= cclang('new'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Pengajuan Training</h3>
                            <h5 class="widget-user-desc"><?= cclang('new', ['Pengajuan Training']); ?></h5>
                            <hr>
                        </div>
                        <?= form_open('', [
                            'name'    => 'form_pengajuan_training', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_pengajuan_training', 
                            'enctype' => 'multipart/form-data', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="nama_pemohon" class="col-sm-3 control-label">Nama Pemohon 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="nama_pemohon" id="nama_pemohon" data-placeholder="Nama Pemohon" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('employee') as $row): ?>
                                    <option value="<?= $row->employee_id ?>"><?= $row->employee_name; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="calon_peserta" class="col-sm-3 control-label">Calon Peserta 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="calon_peserta[]" id="calon_peserta" data-placeholder="Calon Peserta" multiple >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('employee') as $row): ?>
                                    <option value="<?= $row->employee_id ?>"><?= $row->employee_name; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="jenis_training" class="col-sm-3 control-label">Jenis Training 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="jenis_training" id="jenis_training" data-placeholder="Jenis Training" >
                                    <option value=""></option>
                                    <option value="Technical Skill Training">Technical Skill Training</option>
                                    <option value="Soft Skill Training">Soft Skill Training</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="pengadaan_training" class="col-sm-3 control-label">Pengadaan Training 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="pengadaan_training" id="pengadaan_training" data-placeholder="Pengadaan Training" >
                                    <option value=""></option>
                                    <option value="Training Eksternal">Training Eksternal</option>
                                    <option value="Trainning Internal">Training Internal</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nama_training" class="col-sm-3 control-label">Nama Training 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nama_training" id="nama_training" placeholder="Nama Training" value="<?= set_value('nama_training'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="deskripsi_training" class="col-sm-3 control-label">Deskripsi Training 
                            </label>
                            <div class="col-sm-8">
                                <textarea id="deskripsi_training" name="deskripsi_training" rows="5" class="textarea"><?= set_value('deskripsi_training'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="tujuan_training" class="col-sm-3 control-label">Tujuan Training 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="tujuan_training" id="tujuan_training" data-placeholder="Tujuan Training" >
                                    <option value=""></option>
                                    <option value="Perbaikan Kinerja">Perbaikan Kinerja</option>
                                    <option value="Pengembangan Keahlian">Pengembangan Keahlian</option>
                                    <option value="Penambahan Pengetahuan">Penambahan Pengetahuan</option>
                                    <option value="Team Building">Team Building</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="alasan_pengajuan" class="col-sm-3 control-label">Alasan Pengajuan 
                            </label>
                            <div class="col-sm-8">
                                <textarea id="alasan_pengajuan" name="alasan_pengajuan" rows="5" class="textarea"><?= set_value('alasan_pengajuan'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="nama_trainer" class="col-sm-3 control-label">Nama Trainer 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nama_trainer" id="nama_trainer" placeholder="Nama Trainer" value="<?= set_value('nama_trainer'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="lokasi_training" class="col-sm-3 control-label">Lokasi Training 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="lokasi_training" id="lokasi_training" data-placeholder="Lokasi Training" >
                                    <option value=""></option>
                                    <option value="Kantor">Kantor</option>
                                    <option value="Lainnya">Lainnya</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="lembaga_pelaksana" class="col-sm-3 control-label">Lembaga Pelaksana 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="lembaga_pelaksana" id="lembaga_pelaksana" placeholder="Lembaga Pelaksana" value="<?= set_value('lembaga_pelaksana'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="tgl_mulai_training" class="col-sm-3 control-label">Tgl Mulai Training 
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datetimepicker" name="tgl_mulai_training"  id="tgl_mulai_training">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="tgl_selesai_training" class="col-sm-3 control-label">Tgl Selesai Training 
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datetimepicker" name="tgl_selesai_training"  id="tgl_selesai_training">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="lama_training" class="col-sm-3 control-label">Lama Training 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="lama_training" id="lama_training" data-placeholder="Lama Training" >
                                    <option value=""></option>
                                    <option value="Half Day">Half Day</option>
                                    <option value="Full Day">Full Day</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="jumlah_investasi" class="col-sm-3 control-label">Jumlah Investasi 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="jumlah_investasi" id="jumlah_investasi" placeholder="Jumlah Investasi" value="<?= set_value('jumlah_investasi'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="jenis_pembayaran" class="col-sm-3 control-label">Jenis Pembayaran 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="jenis_pembayaran" id="jenis_pembayaran" data-placeholder="Jenis Pembayaran" >
                                    <option value=""></option>
                                    <option value="Transfer">Transfer</option>
                                    <option value="Cash">Cash</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="harapan_training" class="col-sm-3 control-label">Harapan Training 
                            </label>
                            <div class="col-sm-8">
                                <textarea id="harapan_training" name="harapan_training" rows="5" class="textarea"><?= set_value('harapan_training'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="rencana_implementasi" class="col-sm-3 control-label">Rencana Implementasi 
                            </label>
                            <div class="col-sm-8">
                                <textarea id="rencana_implementasi" name="rencana_implementasi" rows="5" class="textarea"><?= set_value('rencana_implementasi'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="sharing_knowledge" class="col-sm-3 control-label">Sharing Knowledge 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="sharing_knowledge" id="sharing_knowledge" data-placeholder="Sharing Knowledge" >
                                    <option value=""></option>
                                    <option value="Bersedia">Bersedia</option>
                                    <option value="Tidak Bersedia">Tidak Bersedia</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                           <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
                   
      $('#btn_cancel').click(function(){
        swal({
            title: "<?= cclang('are_you_sure'); ?>",
            text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/pengajuan_training';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_pengajuan_training = $('#form_pengajuan_training');
        var data_post = form_pengajuan_training.serializeArray();
        var save_type = $(this).attr('data-stype');

        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: BASE_URL + '/administrator/pengajuan_training/add_save',
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            resetForm();
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
                
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
       
 
       
    
    
    }); /*end doc ready*/
</script>