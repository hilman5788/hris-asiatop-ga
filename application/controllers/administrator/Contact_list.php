<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Contact List Controller
*| --------------------------------------------------------------------------
*| Contact List site
*|
*/
class Contact_list extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_contact_list');
	}

	/**
	* show all Contact Lists
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('contact_list_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['contact_lists'] = $this->model_contact_list->get($filter, $field, $this->limit_page, $offset);
		$this->data['contact_list_counts'] = $this->model_contact_list->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/contact_list/index/',
			'total_rows'   => $this->model_contact_list->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Contact List List');
		$this->render('backend/standard/administrator/contact_list/contact_list_list', $this->data);
	}
	
	/**
	* Add new contact_lists
	*
	*/
	public function add()
	{
		$this->is_allowed('contact_list_add');

		$this->template->title('Contact List New');
		$this->render('backend/standard/administrator/contact_list/contact_list_add', $this->data);
	}

	/**
	* Add New Contact Lists
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('contact_list_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('contactlist_name', 'Contactlist Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('street_address', 'Street Address', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('zip', 'Zip', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'contactlist_name' => $this->input->post('contactlist_name'),
				'street_address' => $this->input->post('street_address'),
				'city' => $this->input->post('city'),
				'description' => $this->input->post('description'),
				'zip' => $this->input->post('zip'),
				'phone_number' => $this->input->post('phone_number'),
			];

			
			$save_contact_list = $this->model_contact_list->store($save_data);

			if ($save_contact_list) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_contact_list;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/contact_list/edit/' . $save_contact_list, 'Edit Contact List'),
						anchor('administrator/contact_list', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/contact_list/edit/' . $save_contact_list, 'Edit Contact List')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/contact_list');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/contact_list');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Contact Lists
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('contact_list_update');

		$this->data['contact_list'] = $this->model_contact_list->find($id);

		$this->template->title('Contact List Update');
		$this->render('backend/standard/administrator/contact_list/contact_list_update', $this->data);
	}

	/**
	* Update Contact Lists
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('contact_list_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('contactlist_name', 'Contactlist Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('street_address', 'Street Address', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('zip', 'Zip', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'contactlist_name' => $this->input->post('contactlist_name'),
				'street_address' => $this->input->post('street_address'),
				'city' => $this->input->post('city'),
				'description' => $this->input->post('description'),
				'zip' => $this->input->post('zip'),
				'phone_number' => $this->input->post('phone_number'),
			];

			
			$save_contact_list = $this->model_contact_list->change($id, $save_data);

			if ($save_contact_list) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/contact_list', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/contact_list');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/contact_list');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Contact Lists
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('contact_list_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'contact_list'), 'success');
        } else {
            set_message(cclang('error_delete', 'contact_list'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Contact Lists
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('contact_list_view');

		$this->data['contact_list'] = $this->model_contact_list->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Contact List Detail');
		$this->render('backend/standard/administrator/contact_list/contact_list_view', $this->data);
	}
	
	/**
	* delete Contact Lists
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$contact_list = $this->model_contact_list->find($id);

		
		
		return $this->model_contact_list->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('contact_list_export');

		$this->model_contact_list->export('contact_list', 'contact_list');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('contact_list_export');

		$this->model_contact_list->pdf('contact_list', 'contact_list');
	}
}


/* End of file contact_list.php */
/* Location: ./application/controllers/administrator/Contact List.php */