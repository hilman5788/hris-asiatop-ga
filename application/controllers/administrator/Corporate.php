<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Corporate Controller
*| --------------------------------------------------------------------------
*| Corporate site
*|
*/
class Corporate extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_corporate');
	}

	/**
	* show all Corporates
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('corporate_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['corporates'] = $this->model_corporate->get($filter, $field, $this->limit_page, $offset);
		$this->data['corporate_counts'] = $this->model_corporate->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/corporate/index/',
			'total_rows'   => $this->model_corporate->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Corporate List');
		$this->render('backend/standard/administrator/corporate/corporate_list', $this->data);
	}
	
	/**
	* Add new corporates
	*
	*/
	public function add()
	{
		$this->is_allowed('corporate_add');

		$this->template->title('Corporate New');
		$this->render('backend/standard/administrator/corporate/corporate_add', $this->data);
	}

	/**
	* Add New Corporates
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('corporate_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('corporate_name', 'Corporate Name', 'trim|required');
		$this->form_validation->set_rules('corporate_grade', 'Grade Level', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'corporate_name' => $this->input->post('corporate_name'),
				'corporate_grade' => $this->input->post('corporate_grade'),
				'corporate_rank' => $this->input->post('corporate_rank'),
				'corporate_job_title' => $this->input->post('corporate_job_title'),
			];

			
			$save_corporate = $this->model_corporate->store($save_data);

			if ($save_corporate) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_corporate;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/corporate/edit/' . $save_corporate, 'Edit Corporate'),
						anchor('administrator/corporate', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/corporate/edit/' . $save_corporate, 'Edit Corporate')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/corporate');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/corporate');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Corporates
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('corporate_update');

		$this->data['corporate'] = $this->model_corporate->find($id);

		$this->template->title('Corporate Update');
		$this->render('backend/standard/administrator/corporate/corporate_update', $this->data);
	}

	/**
	* Update Corporates
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('corporate_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('corporate_name', 'Corporate Name', 'trim|required');
		$this->form_validation->set_rules('corporate_grade', 'Grade Level', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'corporate_name' => $this->input->post('corporate_name'),
				'corporate_grade' => $this->input->post('corporate_grade'),
				'corporate_rank' => $this->input->post('corporate_rank'),
				'corporate_job_title' => $this->input->post('corporate_job_title'),
			];

			
			$save_corporate = $this->model_corporate->change($id, $save_data);

			if ($save_corporate) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/corporate', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/corporate');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/corporate');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Corporates
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('corporate_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'corporate'), 'success');
        } else {
            set_message(cclang('error_delete', 'corporate'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Corporates
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('corporate_view');

		$this->data['corporate'] = $this->model_corporate->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Corporate Detail');
		$this->render('backend/standard/administrator/corporate/corporate_view', $this->data);
	}
	
	/**
	* delete Corporates
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$corporate = $this->model_corporate->find($id);

		
		
		return $this->model_corporate->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('corporate_export');

		$this->model_corporate->export('corporate', 'corporate');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('corporate_export');

		$this->model_corporate->pdf('corporate', 'corporate');
	}
}


/* End of file corporate.php */
/* Location: ./application/controllers/administrator/Corporate.php */