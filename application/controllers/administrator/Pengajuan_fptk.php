<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Pengajuan Fptk Controller
*| --------------------------------------------------------------------------
*| Pengajuan Fptk site
*|
*/
class Pengajuan_fptk extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_pengajuan_fptk');
	}

	/**
	* show all Pengajuan Fptks
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('pengajuan_fptk_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['pengajuan_fptks'] = $this->model_pengajuan_fptk->get($filter, $field, $this->limit_page, $offset);
		$this->data['pengajuan_fptk_counts'] = $this->model_pengajuan_fptk->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/pengajuan_fptk/index/',
			'total_rows'   => $this->model_pengajuan_fptk->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Pengajuan Fptk List');
		$this->render('backend/standard/administrator/pengajuan_fptk/pengajuan_fptk_list', $this->data);
	}
	
	/**
	* Add new pengajuan_fptks
	*
	*/
	public function add()
	{
		$this->is_allowed('pengajuan_fptk_add');

		$this->template->title('Pengajuan Fptk New');
		$this->render('backend/standard/administrator/pengajuan_fptk/pengajuan_fptk_add', $this->data);
	}

	/**
	* Add New Pengajuan Fptks
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('pengajuan_fptk_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'requester_id' => get_user_data('id'),				'jabatan' => $this->input->post('jabatan'),
				'jumlah' => $this->input->post('jumlah'),
				'status_dibutuhkan' => $this->input->post('status_dibutuhkan'),
				'atasan_langsung' => $this->input->post('atasan_langsung'),
				'alasan_permohonan' => $this->input->post('alasan_permohonan'),
				'penempatan' => $this->input->post('penempatan'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'usia' => $this->input->post('usia'),
				'status_karyawan' => $this->input->post('status_karyawan'),
				'marital_status' => $this->input->post('marital_status'),
				'pendidikan' => $this->input->post('pendidikan'),
				'pengalaman' => $this->input->post('pengalaman'),
				'keterampilan' => implode(',', (array) $this->input->post('keterampilan')),
			];

			
			$save_pengajuan_fptk = $this->model_pengajuan_fptk->store($save_data);

			if ($save_pengajuan_fptk) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_pengajuan_fptk;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/pengajuan_fptk/edit/' . $save_pengajuan_fptk, 'Edit Pengajuan Fptk'),
						anchor('administrator/pengajuan_fptk', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/pengajuan_fptk/edit/' . $save_pengajuan_fptk, 'Edit Pengajuan Fptk')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/pengajuan_fptk');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/pengajuan_fptk');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Pengajuan Fptks
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('pengajuan_fptk_update');

		$this->data['pengajuan_fptk'] = $this->model_pengajuan_fptk->find($id);

		$this->template->title('Pengajuan Fptk Update');
		$this->render('backend/standard/administrator/pengajuan_fptk/pengajuan_fptk_update', $this->data);
	}

	/**
	* Update Pengajuan Fptks
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('pengajuan_fptk_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'requester_id' => get_user_data('id'),				'jabatan' => $this->input->post('jabatan'),
				'jumlah' => $this->input->post('jumlah'),
				'status_dibutuhkan' => $this->input->post('status_dibutuhkan'),
				'atasan_langsung' => $this->input->post('atasan_langsung'),
				'alasan_permohonan' => $this->input->post('alasan_permohonan'),
				'penempatan' => $this->input->post('penempatan'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'usia' => $this->input->post('usia'),
				'status_karyawan' => $this->input->post('status_karyawan'),
				'marital_status' => $this->input->post('marital_status'),
				'pendidikan' => $this->input->post('pendidikan'),
				'pengalaman' => $this->input->post('pengalaman'),
				'keterampilan' => implode(',', (array) $this->input->post('keterampilan')),
			];

			
			$save_pengajuan_fptk = $this->model_pengajuan_fptk->change($id, $save_data);

			if ($save_pengajuan_fptk) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/pengajuan_fptk', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/pengajuan_fptk');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/pengajuan_fptk');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Pengajuan Fptks
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('pengajuan_fptk_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'pengajuan_fptk'), 'success');
        } else {
            set_message(cclang('error_delete', 'pengajuan_fptk'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Pengajuan Fptks
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('pengajuan_fptk_view');

		$this->data['pengajuan_fptk'] = $this->model_pengajuan_fptk->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Pengajuan Fptk Detail');
		$this->render('backend/standard/administrator/pengajuan_fptk/pengajuan_fptk_view', $this->data);
	}
	
	/**
	* delete Pengajuan Fptks
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$pengajuan_fptk = $this->model_pengajuan_fptk->find($id);

		
		
		return $this->model_pengajuan_fptk->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('pengajuan_fptk_export');

		$this->model_pengajuan_fptk->export('pengajuan_fptk', 'pengajuan_fptk');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('pengajuan_fptk_export');

		$this->model_pengajuan_fptk->pdf('pengajuan_fptk', 'pengajuan_fptk');
	}
}


/* End of file pengajuan_fptk.php */
/* Location: ./application/controllers/administrator/Pengajuan Fptk.php */