<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Section Controller
*| --------------------------------------------------------------------------
*| Section site
*|
*/
class Section extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_section');
	}

	/**
	* show all Sections
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('section_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['sections'] = $this->model_section->get($filter, $field, $this->limit_page, $offset);
		$this->data['section_counts'] = $this->model_section->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/section/index/',
			'total_rows'   => $this->model_section->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Section List');
		$this->render('backend/standard/administrator/section/section_list', $this->data);
	}
	
	/**
	* Add new sections
	*
	*/
	public function add()
	{
		$this->is_allowed('section_add');

		$this->template->title('Section New');
		$this->render('backend/standard/administrator/section/section_add', $this->data);
	}

	/**
	* Add New Sections
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('section_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('section_name', 'Section Name', 'trim|required');
		$this->form_validation->set_rules('section_grade', 'Grade Level', 'trim|required');
		$this->form_validation->set_rules('department_id', 'Department Name', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'section_name' => $this->input->post('section_name'),
				'section_grade' => $this->input->post('section_grade'),
				'section_rank' => $this->input->post('section_rank'),
				'section_title' => $this->input->post('section_title'),
				'section_job_title' => $this->input->post('section_job_title'),
				'department_id' => $this->input->post('department_id'),
			];

			
			$save_section = $this->model_section->store($save_data);

			if ($save_section) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_section;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/section/edit/' . $save_section, 'Edit Section'),
						anchor('administrator/section', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/section/edit/' . $save_section, 'Edit Section')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/section');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/section');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Sections
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('section_update');

		$this->data['section'] = $this->model_section->find($id);

		$this->template->title('Section Update');
		$this->render('backend/standard/administrator/section/section_update', $this->data);
	}

	/**
	* Update Sections
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('section_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('section_name', 'Section Name', 'trim|required');
		$this->form_validation->set_rules('section_grade', 'Grade Level', 'trim|required');
		$this->form_validation->set_rules('department_id', 'Department Name', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'section_name' => $this->input->post('section_name'),
				'section_grade' => $this->input->post('section_grade'),
				'section_rank' => $this->input->post('section_rank'),
				'section_title' => $this->input->post('section_title'),
				'section_job_title' => $this->input->post('section_job_title'),
				'department_id' => $this->input->post('department_id'),
			];

			
			$save_section = $this->model_section->change($id, $save_data);

			if ($save_section) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/section', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/section');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/section');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Sections
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('section_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'section'), 'success');
        } else {
            set_message(cclang('error_delete', 'section'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Sections
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('section_view');

		$this->data['section'] = $this->model_section->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Section Detail');
		$this->render('backend/standard/administrator/section/section_view', $this->data);
	}
	
	/**
	* delete Sections
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$section = $this->model_section->find($id);

		
		
		return $this->model_section->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('section_export');

		$this->model_section->export('section', 'section');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('section_export');

		$this->model_section->pdf('section', 'section');
	}
}


/* End of file section.php */
/* Location: ./application/controllers/administrator/Section.php */