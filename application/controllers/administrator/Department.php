<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Department Controller
*| --------------------------------------------------------------------------
*| Department site
*|
*/
class Department extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_department');
	}

	/**
	* show all Departments
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('department_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['departments'] = $this->model_department->get($filter, $field, $this->limit_page, $offset);
		$this->data['department_counts'] = $this->model_department->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/department/index/',
			'total_rows'   => $this->model_department->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Department List');
		$this->render('backend/standard/administrator/department/department_list', $this->data);
	}
	
	/**
	* Add new departments
	*
	*/
	public function add()
	{
		$this->is_allowed('department_add');

		$this->template->title('Department New');
		$this->render('backend/standard/administrator/department/department_add', $this->data);
	}

	/**
	* Add New Departments
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('department_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('department_name', 'Department Name', 'trim|required');
		$this->form_validation->set_rules('department_grade', 'Grade Level', 'trim|required');
		$this->form_validation->set_rules('division_id', 'Division Name', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'department_name' => $this->input->post('department_name'),
				'department_grade' => $this->input->post('department_grade'),
				'department_rank' => $this->input->post('department_rank'),
				'department_title' => $this->input->post('department_title'),
				'department_job_title' => $this->input->post('department_job_title'),
				'division_id' => $this->input->post('division_id'),
			];

			
			$save_department = $this->model_department->store($save_data);

			if ($save_department) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_department;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/department/edit/' . $save_department, 'Edit Department'),
						anchor('administrator/department', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/department/edit/' . $save_department, 'Edit Department')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/department');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/department');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Departments
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('department_update');

		$this->data['department'] = $this->model_department->find($id);

		$this->template->title('Department Update');
		$this->render('backend/standard/administrator/department/department_update', $this->data);
	}

	/**
	* Update Departments
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('department_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('department_name', 'Department Name', 'trim|required');
		$this->form_validation->set_rules('department_grade', 'Grade Level', 'trim|required');
		$this->form_validation->set_rules('division_id', 'Division Name', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'department_name' => $this->input->post('department_name'),
				'department_grade' => $this->input->post('department_grade'),
				'department_rank' => $this->input->post('department_rank'),
				'department_title' => $this->input->post('department_title'),
				'department_job_title' => $this->input->post('department_job_title'),
				'division_id' => $this->input->post('division_id'),
			];

			
			$save_department = $this->model_department->change($id, $save_data);

			if ($save_department) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/department', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/department');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/department');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Departments
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('department_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'department'), 'success');
        } else {
            set_message(cclang('error_delete', 'department'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Departments
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('department_view');

		$this->data['department'] = $this->model_department->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Department Detail');
		$this->render('backend/standard/administrator/department/department_view', $this->data);
	}
	
	/**
	* delete Departments
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$department = $this->model_department->find($id);

		
		
		return $this->model_department->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('department_export');

		$this->model_department->export('department', 'department');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('department_export');

		$this->model_department->pdf('department', 'department');
	}
}


/* End of file department.php */
/* Location: ./application/controllers/administrator/Department.php */