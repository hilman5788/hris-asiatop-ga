<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Officer Controller
*| --------------------------------------------------------------------------
*| Officer site
*|
*/
class Officer extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_officer');
	}

	/**
	* show all Officers
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('officer_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['officers'] = $this->model_officer->get($filter, $field, $this->limit_page, $offset);
		$this->data['officer_counts'] = $this->model_officer->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/officer/index/',
			'total_rows'   => $this->model_officer->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Officer List');
		$this->render('backend/standard/administrator/officer/officer_list', $this->data);
	}
	
	/**
	* Add new officers
	*
	*/
	public function add()
	{
		$this->is_allowed('officer_add');

		$this->template->title('Officer New');
		$this->render('backend/standard/administrator/officer/officer_add', $this->data);
	}

	/**
	* Add New Officers
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('officer_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('officer_name', 'Officer Name', 'trim|required');
		$this->form_validation->set_rules('officer_grade', 'Grade Level', 'trim|required');
		$this->form_validation->set_rules('supervisor_id', 'Supervisor Name', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'officer_name' => $this->input->post('officer_name'),
				'officer_grade' => $this->input->post('officer_grade'),
				'officer_rank' => $this->input->post('officer_rank'),
				'officer_title' => $this->input->post('officer_title'),
				'officer_job_title' => $this->input->post('officer_job_title'),
				'supervisor_id' => $this->input->post('supervisor_id'),
			];

			
			$save_officer = $this->model_officer->store($save_data);

			if ($save_officer) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_officer;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/officer/edit/' . $save_officer, 'Edit Officer'),
						anchor('administrator/officer', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/officer/edit/' . $save_officer, 'Edit Officer')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/officer');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/officer');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Officers
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('officer_update');

		$this->data['officer'] = $this->model_officer->find($id);

		$this->template->title('Officer Update');
		$this->render('backend/standard/administrator/officer/officer_update', $this->data);
	}

	/**
	* Update Officers
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('officer_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('officer_name', 'Officer Name', 'trim|required');
		$this->form_validation->set_rules('officer_grade', 'Grade Level', 'trim|required');
		$this->form_validation->set_rules('supervisor_id', 'Supervisor Name', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'officer_name' => $this->input->post('officer_name'),
				'officer_grade' => $this->input->post('officer_grade'),
				'officer_rank' => $this->input->post('officer_rank'),
				'officer_title' => $this->input->post('officer_title'),
				'officer_job_title' => $this->input->post('officer_job_title'),
				'supervisor_id' => $this->input->post('supervisor_id'),
			];

			
			$save_officer = $this->model_officer->change($id, $save_data);

			if ($save_officer) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/officer', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/officer');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/officer');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Officers
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('officer_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'officer'), 'success');
        } else {
            set_message(cclang('error_delete', 'officer'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Officers
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('officer_view');

		$this->data['officer'] = $this->model_officer->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Officer Detail');
		$this->render('backend/standard/administrator/officer/officer_view', $this->data);
	}
	
	/**
	* delete Officers
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$officer = $this->model_officer->find($id);

		
		
		return $this->model_officer->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('officer_export');

		$this->model_officer->export('officer', 'officer');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('officer_export');

		$this->model_officer->pdf('officer', 'officer');
	}
}


/* End of file officer.php */
/* Location: ./application/controllers/administrator/Officer.php */