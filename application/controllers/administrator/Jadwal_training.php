<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Jadwal Training Controller
*| --------------------------------------------------------------------------
*| Jadwal Training site
*|
*/
class Jadwal_training extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_jadwal_training');
	}

	/**
	* show all Jadwal Trainings
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('jadwal_training_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['jadwal_trainings'] = $this->model_jadwal_training->get($filter, $field, $this->limit_page, $offset);
		$this->data['jadwal_training_counts'] = $this->model_jadwal_training->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/jadwal_training/index/',
			'total_rows'   => $this->model_jadwal_training->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Jadwal Training List');
		$this->render('backend/standard/administrator/jadwal_training/jadwal_training_list', $this->data);
	}
	
	/**
	* Add new jadwal_trainings
	*
	*/
	public function add()
	{
		$this->is_allowed('jadwal_training_add');

		$this->template->title('Jadwal Training New');
		$this->render('backend/standard/administrator/jadwal_training/jadwal_training_add', $this->data);
	}

	/**
	* Add New Jadwal Trainings
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('jadwal_training_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('nama_training', 'Nama Training', 'trim|required');
		$this->form_validation->set_rules('tanggal_mulai_training', 'Tanggal Mulai Training', 'trim|required');
		$this->form_validation->set_rules('tanggal_evaluasi', 'Tanggal Evaluasi', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'nama_training' => $this->input->post('nama_training'),
				'tanggal_mulai_training' => $this->input->post('tanggal_mulai_training'),
				'tanggal_evaluasi' => $this->input->post('tanggal_evaluasi'),
				'status' => $this->input->post('status'),
				'url_evaluasi_tertulis' => $this->input->post('url_evaluasi_tertulis'),
				'nama_trainer' => $this->input->post('nama_trainer'),
				'penyelenggara' => $this->input->post('penyelenggara'),
			];

			
			$save_jadwal_training = $this->model_jadwal_training->store($save_data);

			if ($save_jadwal_training) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_jadwal_training;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/jadwal_training/edit/' . $save_jadwal_training, 'Edit Jadwal Training'),
						anchor('administrator/jadwal_training', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/jadwal_training/edit/' . $save_jadwal_training, 'Edit Jadwal Training')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/jadwal_training');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/jadwal_training');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Jadwal Trainings
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('jadwal_training_update');

		$this->data['jadwal_training'] = $this->model_jadwal_training->find($id);

		$this->template->title('Jadwal Training Update');
		$this->render('backend/standard/administrator/jadwal_training/jadwal_training_update', $this->data);
	}

	/**
	* Update Jadwal Trainings
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('jadwal_training_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('nama_training', 'Nama Training', 'trim|required');
		$this->form_validation->set_rules('tanggal_mulai_training', 'Tanggal Mulai Training', 'trim|required');
		$this->form_validation->set_rules('tanggal_evaluasi', 'Tanggal Evaluasi', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'nama_training' => $this->input->post('nama_training'),
				'tanggal_mulai_training' => $this->input->post('tanggal_mulai_training'),
				'tanggal_evaluasi' => $this->input->post('tanggal_evaluasi'),
				'status' => $this->input->post('status'),
				'url_evaluasi_tertulis' => $this->input->post('url_evaluasi_tertulis'),
				'nama_trainer' => $this->input->post('nama_trainer'),
				'penyelenggara' => $this->input->post('penyelenggara'),
			];

			
			$save_jadwal_training = $this->model_jadwal_training->change($id, $save_data);

			if ($save_jadwal_training) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/jadwal_training', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/jadwal_training');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/jadwal_training');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Jadwal Trainings
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('jadwal_training_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'jadwal_training'), 'success');
        } else {
            set_message(cclang('error_delete', 'jadwal_training'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Jadwal Trainings
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('jadwal_training_view');

		$this->data['jadwal_training'] = $this->model_jadwal_training->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Jadwal Training Detail');
		$this->render('backend/standard/administrator/jadwal_training/jadwal_training_view', $this->data);
	}
	
	/**
	* delete Jadwal Trainings
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$jadwal_training = $this->model_jadwal_training->find($id);

		
		
		return $this->model_jadwal_training->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('jadwal_training_export');

		$this->model_jadwal_training->export('jadwal_training', 'jadwal_training');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('jadwal_training_export');

		$this->model_jadwal_training->pdf('jadwal_training', 'jadwal_training');
	}
}


/* End of file jadwal_training.php */
/* Location: ./application/controllers/administrator/Jadwal Training.php */