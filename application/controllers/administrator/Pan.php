<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Pan Controller
*| --------------------------------------------------------------------------
*| Pan site
*|
*/
class Pan extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_pan');
	}

	/**
	* show all Pans
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('pan_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['pans'] = $this->model_pan->get($filter, $field, $this->limit_page, $offset);
		$this->data['pan_counts'] = $this->model_pan->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/pan/index/',
			'total_rows'   => $this->model_pan->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('PAN List');
		$this->render('backend/standard/administrator/pan/pan_list', $this->data);
	}
	
	/**
	* Add new pans
	*
	*/
	public function add()
	{
		$this->is_allowed('pan_add');

		$this->template->title('PAN New');
		$this->render('backend/standard/administrator/pan/pan_add', $this->data);
	}

	/**
	* Add New Pans
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('pan_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('nama_karyawan', 'Nama Karyawan', 'trim|required');
		$this->form_validation->set_rules('jenis_usulan', 'Jenis Usulan', 'trim|required');
		$this->form_validation->set_rules('dasar_usulan', 'Dasar Usulan', 'trim|required');
		$this->form_validation->set_rules('lampiran_pendukung[]', 'Lampiran Pendukung', 'trim|required');
		$this->form_validation->set_rules('pencapaian_KPI', 'Pencapaian KPI', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'nama_karyawan' => $this->input->post('nama_karyawan'),
				'jenis_usulan' => $this->input->post('jenis_usulan'),
				'tgl_berlaku_mulai' => $this->input->post('tgl_berlaku_mulai'),
				'tgl_berlaku_selesai' => $this->input->post('tgl_berlaku_selesai'),
				'perubahan_dari' => $this->input->post('perubahan_dari'),
				'perubahan_menjadi' => $this->input->post('perubahan_menjadi'),
				'dasar_usulan' => $this->input->post('dasar_usulan'),
				'lampiran_pendukung' => implode(',', (array) $this->input->post('lampiran_pendukung')),
				'pencapaian_KPI' => $this->input->post('pencapaian_KPI'),
			];

			
			$save_pan = $this->model_pan->store($save_data);

			if ($save_pan) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_pan;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/pan/edit/' . $save_pan, 'Edit Pan'),
						anchor('administrator/pan', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/pan/edit/' . $save_pan, 'Edit Pan')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/pan');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/pan');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Pans
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('pan_update');

		$this->data['pan'] = $this->model_pan->find($id);

		$this->template->title('PAN Update');
		$this->render('backend/standard/administrator/pan/pan_update', $this->data);
	}

	/**
	* Update Pans
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('pan_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('nama_karyawan', 'Nama Karyawan', 'trim|required');
		$this->form_validation->set_rules('jenis_usulan', 'Jenis Usulan', 'trim|required');
		$this->form_validation->set_rules('dasar_usulan', 'Dasar Usulan', 'trim|required');
		$this->form_validation->set_rules('lampiran_pendukung[]', 'Lampiran Pendukung', 'trim|required');
		$this->form_validation->set_rules('pencapaian_KPI', 'Pencapaian KPI', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'nama_karyawan' => $this->input->post('nama_karyawan'),
				'jenis_usulan' => $this->input->post('jenis_usulan'),
				'tgl_berlaku_mulai' => $this->input->post('tgl_berlaku_mulai'),
				'tgl_berlaku_selesai' => $this->input->post('tgl_berlaku_selesai'),
				'perubahan_dari' => $this->input->post('perubahan_dari'),
				'perubahan_menjadi' => $this->input->post('perubahan_menjadi'),
				'dasar_usulan' => $this->input->post('dasar_usulan'),
				'lampiran_pendukung' => implode(',', (array) $this->input->post('lampiran_pendukung')),
				'pencapaian_KPI' => $this->input->post('pencapaian_KPI'),
			];

			
			$save_pan = $this->model_pan->change($id, $save_data);

			if ($save_pan) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/pan', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/pan');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/pan');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Pans
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('pan_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'pan'), 'success');
        } else {
            set_message(cclang('error_delete', 'pan'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Pans
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('pan_view');

		$this->data['pan'] = $this->model_pan->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('PAN Detail');
		$this->render('backend/standard/administrator/pan/pan_view', $this->data);
	}
	
	/**
	* delete Pans
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$pan = $this->model_pan->find($id);

		
		
		return $this->model_pan->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('pan_export');

		$this->model_pan->export('pan', 'pan');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('pan_export');

		$this->model_pan->pdf('pan', 'pan');
	}
}


/* End of file pan.php */
/* Location: ./application/controllers/administrator/Pan.php */