<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Position Controller
*| --------------------------------------------------------------------------
*| Position site
*|
*/
class Position extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_position');
	}

	/**
	* show all Positions
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('position_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['positions'] = $this->model_position->get($filter, $field, $this->limit_page, $offset);
		$this->data['position_counts'] = $this->model_position->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/position/index/',
			'total_rows'   => $this->model_position->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Position List');
		$this->render('backend/standard/administrator/position/position_list', $this->data);
	}
	
	/**
	* Add new positions
	*
	*/
	public function add()
	{
		$this->is_allowed('position_add');

		$this->template->title('Position New');
		$this->render('backend/standard/administrator/position/position_add', $this->data);
	}

	/**
	* Add New Positions
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('position_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('position_name', 'Position Name', 'trim|required');
		$this->form_validation->set_rules('position_department_id', 'Department', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'position_name' => $this->input->post('position_name'),
				'position_department_id' => $this->input->post('position_department_id'),
				'position_desc' => $this->input->post('position_desc'),
			];

			
			$save_position = $this->model_position->store($save_data);

			if ($save_position) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_position;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/position/edit/' . $save_position, 'Edit Position'),
						anchor('administrator/position', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/position/edit/' . $save_position, 'Edit Position')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/position');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/position');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Positions
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('position_update');

		$this->data['position'] = $this->model_position->find($id);

		$this->template->title('Position Update');
		$this->render('backend/standard/administrator/position/position_update', $this->data);
	}

	/**
	* Update Positions
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('position_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('position_name', 'Position Name', 'trim|required');
		$this->form_validation->set_rules('position_department_id', 'Department', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'position_name' => $this->input->post('position_name'),
				'position_department_id' => $this->input->post('position_department_id'),
				'position_desc' => $this->input->post('position_desc'),
			];

			
			$save_position = $this->model_position->change($id, $save_data);

			if ($save_position) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/position', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/position');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/position');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Positions
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('position_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'position'), 'success');
        } else {
            set_message(cclang('error_delete', 'position'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Positions
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('position_view');

		$this->data['position'] = $this->model_position->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Position Detail');
		$this->render('backend/standard/administrator/position/position_view', $this->data);
	}
	
	/**
	* delete Positions
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$position = $this->model_position->find($id);

		
		
		return $this->model_position->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('position_export');

		$this->model_position->export('position', 'position');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('position_export');

		$this->model_position->pdf('position', 'position');
	}
}


/* End of file position.php */
/* Location: ./application/controllers/administrator/Position.php */