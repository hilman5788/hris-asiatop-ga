<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Employee Controller
*| --------------------------------------------------------------------------
*| Employee site
*|
*/
class Employee extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_employee');
	}

	/**
	* show all Employees
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('employee_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['employees'] = $this->model_employee->get($filter, $field, $this->limit_page, $offset);
		$this->data['employee_counts'] = $this->model_employee->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/employee/index/',
			'total_rows'   => $this->model_employee->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Employee List');
		$this->render('backend/standard/administrator/employee/employee_list', $this->data);
	}
	
	/**
	* Add new employees
	*
	*/
	public function add()
	{
		$this->is_allowed('employee_add');

		$this->template->title('Employee New');
		$this->render('backend/standard/administrator/employee/employee_add', $this->data);
	}

	/**
	* Add New Employees
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('employee_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('employee_nik', 'NIK', 'trim|required');
		$this->form_validation->set_rules('employee_name', 'Employee Name', 'trim|required');
		$this->form_validation->set_rules('employee_hire_date', 'Hire Date', 'trim|required');
		$this->form_validation->set_rules('employee_grade_id', 'Grade', 'trim|required');
		$this->form_validation->set_rules('employee_department_id', 'Department Name', 'trim|required');
		$this->form_validation->set_rules('employee_position_id', 'Position Name', 'trim|required');
		$this->form_validation->set_rules('employee_work_location_id', 'Work Location', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'employee_nik' => $this->input->post('employee_nik'),
				'employee_name' => $this->input->post('employee_name'),
				'employee_hire_date' => $this->input->post('employee_hire_date'),
				'employee_seniority_date' => $this->input->post('employee_seniority_date'),
				'employee_grade_id' => $this->input->post('employee_grade_id'),
				'employee_department_id' => $this->input->post('employee_department_id'),
				'employee_position_id' => $this->input->post('employee_position_id'),
				'employee_work_location_id' => $this->input->post('employee_work_location_id'),
				'employee_sex' => $this->input->post('employee_sex'),
				'employee_birth_place' => $this->input->post('employee_birth_place'),
				'employee_birth_date' => $this->input->post('employee_birth_date'),
				'employee_marital_status' => $this->input->post('employee_marital_status'),
				'employee_religion' => $this->input->post('employee_religion'),
				'employee_social_id' => $this->input->post('employee_social_id'),
				'employee_last_education' => $this->input->post('employee_last_education'),
				'employee_npwp' => $this->input->post('employee_npwp'),
				'employee_blood_type' => $this->input->post('employee_blood_type'),
				'employee_pay_group' => $this->input->post('employee_pay_group'),
				'employee_cost_group' => $this->input->post('employee_cost_group'),
				'employee_tax_category' => $this->input->post('employee_tax_category'),
				'employee_status' => $this->input->post('employee_status'),
				'employee_termination_date' => $this->input->post('employee_termination_date'),
				'employee_contact_phone' => $this->input->post('employee_contact_phone'),
				'employee_contact_fax' => $this->input->post('employee_contact_fax'),
				'employee_address_1' => $this->input->post('employee_address_1'),
				'employee_address_2' => $this->input->post('employee_address_2'),
				'employee_address_rt' => $this->input->post('employee_address_rt'),
				'employee_address_rw' => $this->input->post('employee_address_rw'),
				'employee_address_city' => $this->input->post('employee_address_city'),
				'employee_address_zip_code' => $this->input->post('employee_address_zip_code'),
				'employee_address_state' => $this->input->post('employee_address_state'),
				'employee_bank_account_name' => $this->input->post('employee_bank_account_name'),
				'employee_bank_account_no' => $this->input->post('employee_bank_account_no'),
				'employee_bank_name' => $this->input->post('employee_bank_name'),
			];

			
			$save_employee = $this->model_employee->store($save_data);

			if ($save_employee) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_employee;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/employee/edit/' . $save_employee, 'Edit Employee'),
						anchor('administrator/employee', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/employee/edit/' . $save_employee, 'Edit Employee')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/employee');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/employee');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Employees
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('employee_update');

		$this->data['employee'] = $this->model_employee->find($id);

		$this->template->title('Employee Update');
		$this->render('backend/standard/administrator/employee/employee_update', $this->data);
	}

	/**
	* Update Employees
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('employee_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('employee_nik', 'NIK', 'trim|required');
		$this->form_validation->set_rules('employee_name', 'Employee Name', 'trim|required');
		$this->form_validation->set_rules('employee_hire_date', 'Hire Date', 'trim|required');
		$this->form_validation->set_rules('employee_grade_id', 'Grade', 'trim|required');
		$this->form_validation->set_rules('employee_department_id', 'Department Name', 'trim|required');
		$this->form_validation->set_rules('employee_position_id', 'Position Name', 'trim|required');
		$this->form_validation->set_rules('employee_work_location_id', 'Work Location', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'employee_nik' => $this->input->post('employee_nik'),
				'employee_name' => $this->input->post('employee_name'),
				'employee_hire_date' => $this->input->post('employee_hire_date'),
				'employee_seniority_date' => $this->input->post('employee_seniority_date'),
				'employee_grade_id' => $this->input->post('employee_grade_id'),
				'employee_department_id' => $this->input->post('employee_department_id'),
				'employee_position_id' => $this->input->post('employee_position_id'),
				'employee_work_location_id' => $this->input->post('employee_work_location_id'),
				'employee_sex' => $this->input->post('employee_sex'),
				'employee_birth_place' => $this->input->post('employee_birth_place'),
				'employee_birth_date' => $this->input->post('employee_birth_date'),
				'employee_marital_status' => $this->input->post('employee_marital_status'),
				'employee_religion' => $this->input->post('employee_religion'),
				'employee_social_id' => $this->input->post('employee_social_id'),
				'employee_last_education' => $this->input->post('employee_last_education'),
				'employee_npwp' => $this->input->post('employee_npwp'),
				'employee_blood_type' => $this->input->post('employee_blood_type'),
				'employee_pay_group' => $this->input->post('employee_pay_group'),
				'employee_cost_group' => $this->input->post('employee_cost_group'),
				'employee_tax_category' => $this->input->post('employee_tax_category'),
				'employee_status' => $this->input->post('employee_status'),
				'employee_termination_date' => $this->input->post('employee_termination_date'),
				'employee_contact_phone' => $this->input->post('employee_contact_phone'),
				'employee_contact_fax' => $this->input->post('employee_contact_fax'),
				'employee_address_1' => $this->input->post('employee_address_1'),
				'employee_address_2' => $this->input->post('employee_address_2'),
				'employee_address_rt' => $this->input->post('employee_address_rt'),
				'employee_address_rw' => $this->input->post('employee_address_rw'),
				'employee_address_city' => $this->input->post('employee_address_city'),
				'employee_address_zip_code' => $this->input->post('employee_address_zip_code'),
				'employee_address_state' => $this->input->post('employee_address_state'),
				'employee_bank_account_name' => $this->input->post('employee_bank_account_name'),
				'employee_bank_account_no' => $this->input->post('employee_bank_account_no'),
				'employee_bank_name' => $this->input->post('employee_bank_name'),
			];

			
			$save_employee = $this->model_employee->change($id, $save_data);

			if ($save_employee) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/employee', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/employee');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/employee');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Employees
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('employee_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'employee'), 'success');
        } else {
            set_message(cclang('error_delete', 'employee'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Employees
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('employee_view');

		$this->data['employee'] = $this->model_employee->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Employee Detail');
		$this->render('backend/standard/administrator/employee/employee_view', $this->data);
	}
	
	/**
	* delete Employees
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$employee = $this->model_employee->find($id);

		
		
		return $this->model_employee->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('employee_export');

		$this->model_employee->export('employee', 'employee');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('employee_export');

		$this->model_employee->pdf('employee', 'employee');
	}
}


/* End of file employee.php */
/* Location: ./application/controllers/administrator/Employee.php */