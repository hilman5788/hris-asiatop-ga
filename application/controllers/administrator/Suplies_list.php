<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Suplies List Controller
*| --------------------------------------------------------------------------
*| Suplies List site
*|
*/
class Suplies_list extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_suplies_list');
	}

	/**
	* show all Suplies Lists
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('suplies_list_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['suplies_lists'] = $this->model_suplies_list->get($filter, $field, $this->limit_page, $offset);
		$this->data['suplies_list_counts'] = $this->model_suplies_list->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/suplies_list/index/',
			'total_rows'   => $this->model_suplies_list->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Suplies List List');
		$this->render('backend/standard/administrator/suplies_list/suplies_list_list', $this->data);
	}
	
	/**
	* Add new suplies_lists
	*
	*/
	public function add()
	{
		$this->is_allowed('suplies_list_add');

		$this->template->title('Suplies List New');
		$this->render('backend/standard/administrator/suplies_list/suplies_list_add', $this->data);
	}

	/**
	* Add New Suplies Lists
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('suplies_list_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('suplies_list_name', 'Suplies List Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_name', 'Goods Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('unit', 'Unit', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('item_description', 'Item Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('costumer', 'Costumer', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('zip', 'Zip', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'suplies_list_name' => $this->input->post('suplies_list_name'),
				'goods_name' => $this->input->post('goods_name'),
				'unit' => $this->input->post('unit'),
				'date' => $this->input->post('date'),
				'address' => $this->input->post('address'),
				'item_description' => $this->input->post('item_description'),
				'phone' => $this->input->post('phone'),
				'price' => $this->input->post('price'),
				'costumer' => $this->input->post('costumer'),
				'city' => $this->input->post('city'),
				'zip' => $this->input->post('zip'),
				'quantity' => $this->input->post('quantity'),
			];

			
			$save_suplies_list = $this->model_suplies_list->store($save_data);

			if ($save_suplies_list) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_suplies_list;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/suplies_list/edit/' . $save_suplies_list, 'Edit Suplies List'),
						anchor('administrator/suplies_list', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/suplies_list/edit/' . $save_suplies_list, 'Edit Suplies List')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/suplies_list');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/suplies_list');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Suplies Lists
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('suplies_list_update');

		$this->data['suplies_list'] = $this->model_suplies_list->find($id);

		$this->template->title('Suplies List Update');
		$this->render('backend/standard/administrator/suplies_list/suplies_list_update', $this->data);
	}

	/**
	* Update Suplies Lists
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('suplies_list_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('suplies_list_name', 'Suplies List Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_name', 'Goods Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('unit', 'Unit', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('item_description', 'Item Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('costumer', 'Costumer', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('zip', 'Zip', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'suplies_list_name' => $this->input->post('suplies_list_name'),
				'goods_name' => $this->input->post('goods_name'),
				'unit' => $this->input->post('unit'),
				'date' => $this->input->post('date'),
				'address' => $this->input->post('address'),
				'item_description' => $this->input->post('item_description'),
				'phone' => $this->input->post('phone'),
				'price' => $this->input->post('price'),
				'costumer' => $this->input->post('costumer'),
				'city' => $this->input->post('city'),
				'zip' => $this->input->post('zip'),
				'quantity' => $this->input->post('quantity'),
			];

			
			$save_suplies_list = $this->model_suplies_list->change($id, $save_data);

			if ($save_suplies_list) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/suplies_list', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/suplies_list');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/suplies_list');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Suplies Lists
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('suplies_list_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'suplies_list'), 'success');
        } else {
            set_message(cclang('error_delete', 'suplies_list'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Suplies Lists
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('suplies_list_view');

		$this->data['suplies_list'] = $this->model_suplies_list->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Suplies List Detail');
		$this->render('backend/standard/administrator/suplies_list/suplies_list_view', $this->data);
	}
	
	/**
	* delete Suplies Lists
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$suplies_list = $this->model_suplies_list->find($id);

		
		
		return $this->model_suplies_list->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('suplies_list_export');

		$this->model_suplies_list->export('suplies_list', 'suplies_list');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('suplies_list_export');

		$this->model_suplies_list->pdf('suplies_list', 'suplies_list');
	}
}


/* End of file suplies_list.php */
/* Location: ./application/controllers/administrator/Suplies List.php */