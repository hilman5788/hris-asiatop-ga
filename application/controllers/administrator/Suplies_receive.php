<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Suplies Receive Controller
*| --------------------------------------------------------------------------
*| Suplies Receive site
*|
*/
class Suplies_receive extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_suplies_receive');
	}

	/**
	* show all Suplies Receives
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('suplies_receive_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['suplies_receives'] = $this->model_suplies_receive->get($filter, $field, $this->limit_page, $offset);
		$this->data['suplies_receive_counts'] = $this->model_suplies_receive->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/suplies_receive/index/',
			'total_rows'   => $this->model_suplies_receive->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Suplies Receive List');
		$this->render('backend/standard/administrator/suplies_receive/suplies_receive_list', $this->data);
	}
	
	/**
	* Add new suplies_receives
	*
	*/
	public function add()
	{
		$this->is_allowed('suplies_receive_add');

		$this->template->title('Suplies Receive New');
		$this->render('backend/standard/administrator/suplies_receive/suplies_receive_add', $this->data);
	}

	/**
	* Add New Suplies Receives
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('suplies_receive_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('suplies_receive_name', 'Suplies Receive Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('costumer', 'Costumer', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('zip', 'Zip', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('item_description', 'Item Description', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('goods_name', 'Goods Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('unit', 'Unit', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'suplies_receive_name' => $this->input->post('suplies_receive_name'),
				'description' => $this->input->post('description'),
				'costumer' => $this->input->post('costumer'),
				'address' => $this->input->post('address'),
				'city' => $this->input->post('city'),
				'date' => $this->input->post('date'),
				'phone' => $this->input->post('phone'),
				'zip' => $this->input->post('zip'),
				'quantity' => $this->input->post('quantity'),
				'item_description' => $this->input->post('item_description'),
				'price' => $this->input->post('price'),
				'goods_name' => $this->input->post('goods_name'),
				'unit' => $this->input->post('unit'),
			];

			
			$save_suplies_receive = $this->model_suplies_receive->store($save_data);

			if ($save_suplies_receive) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_suplies_receive;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/suplies_receive/edit/' . $save_suplies_receive, 'Edit Suplies Receive'),
						anchor('administrator/suplies_receive', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/suplies_receive/edit/' . $save_suplies_receive, 'Edit Suplies Receive')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/suplies_receive');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/suplies_receive');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Suplies Receives
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('suplies_receive_update');

		$this->data['suplies_receive'] = $this->model_suplies_receive->find($id);

		$this->template->title('Suplies Receive Update');
		$this->render('backend/standard/administrator/suplies_receive/suplies_receive_update', $this->data);
	}

	/**
	* Update Suplies Receives
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('suplies_receive_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('suplies_receive_name', 'Suplies Receive Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('costumer', 'Costumer', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('zip', 'Zip', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('item_description', 'Item Description', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('goods_name', 'Goods Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('unit', 'Unit', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'suplies_receive_name' => $this->input->post('suplies_receive_name'),
				'description' => $this->input->post('description'),
				'costumer' => $this->input->post('costumer'),
				'address' => $this->input->post('address'),
				'city' => $this->input->post('city'),
				'date' => $this->input->post('date'),
				'phone' => $this->input->post('phone'),
				'zip' => $this->input->post('zip'),
				'quantity' => $this->input->post('quantity'),
				'item_description' => $this->input->post('item_description'),
				'price' => $this->input->post('price'),
				'goods_name' => $this->input->post('goods_name'),
				'unit' => $this->input->post('unit'),
			];

			
			$save_suplies_receive = $this->model_suplies_receive->change($id, $save_data);

			if ($save_suplies_receive) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/suplies_receive', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/suplies_receive');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/suplies_receive');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Suplies Receives
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('suplies_receive_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'suplies_receive'), 'success');
        } else {
            set_message(cclang('error_delete', 'suplies_receive'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Suplies Receives
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('suplies_receive_view');

		$this->data['suplies_receive'] = $this->model_suplies_receive->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Suplies Receive Detail');
		$this->render('backend/standard/administrator/suplies_receive/suplies_receive_view', $this->data);
	}
	
	/**
	* delete Suplies Receives
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$suplies_receive = $this->model_suplies_receive->find($id);

		
		
		return $this->model_suplies_receive->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('suplies_receive_export');

		$this->model_suplies_receive->export('suplies_receive', 'suplies_receive');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('suplies_receive_export');

		$this->model_suplies_receive->pdf('suplies_receive', 'suplies_receive');
	}
}


/* End of file suplies_receive.php */
/* Location: ./application/controllers/administrator/Suplies Receive.php */