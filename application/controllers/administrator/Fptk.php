<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Fptk Controller
*| --------------------------------------------------------------------------
*| Fptk site
*|
*/
class Fptk extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_fptk');
	}

	/**
	* show all Fptks
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('fptk_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['fptks'] = $this->model_fptk->get($filter, $field, $this->limit_page, $offset);
		$this->data['fptk_counts'] = $this->model_fptk->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/fptk/index/',
			'total_rows'   => $this->model_fptk->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('FPTK List');
		$this->render('backend/standard/administrator/fptk/fptk_list', $this->data);
	}
	
	/**
	* Add new fptks
	*
	*/
	public function add()
	{
		$this->is_allowed('fptk_add');

		$this->template->title('FPTK New');
		$this->render('backend/standard/administrator/fptk/fptk_add', $this->data);
	}

	/**
	* Add New Fptks
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('fptk_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('requester_id', 'Requester Id', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('status_dibutuhkan', 'Status Dibutuhkan', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('atasan_langsung', 'Atasan Langsung', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('alasan_permohonan', 'Alasan Permohonan', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('penempatan', 'Penempatan', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('usia', 'Usia', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('status_karyawan', 'Status Karyawan', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('marital_status', 'Marital Status', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('pendidikan', 'Pendidikan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('pengalaman', 'Pengalaman', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('keterampilan', 'Keterampilan', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'requester_id' => $this->input->post('requester_id'),
				'jabatan' => $this->input->post('jabatan'),
				'jumlah' => $this->input->post('jumlah'),
				'status_dibutuhkan' => $this->input->post('status_dibutuhkan'),
				'atasan_langsung' => $this->input->post('atasan_langsung'),
				'alasan_permohonan' => $this->input->post('alasan_permohonan'),
				'penempatan' => $this->input->post('penempatan'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'usia' => $this->input->post('usia'),
				'status_karyawan' => $this->input->post('status_karyawan'),
				'marital_status' => $this->input->post('marital_status'),
				'pendidikan' => $this->input->post('pendidikan'),
				'pengalaman' => $this->input->post('pengalaman'),
				'keterampilan' => $this->input->post('keterampilan'),
			];

			
			$save_fptk = $this->model_fptk->store($save_data);

			if ($save_fptk) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_fptk;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/fptk/edit/' . $save_fptk, 'Edit Fptk'),
						anchor('administrator/fptk', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/fptk/edit/' . $save_fptk, 'Edit Fptk')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/fptk');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/fptk');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Fptks
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('fptk_update');

		$this->data['fptk'] = $this->model_fptk->find($id);

		$this->template->title('FPTK Update');
		$this->render('backend/standard/administrator/fptk/fptk_update', $this->data);
	}

	/**
	* Update Fptks
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('fptk_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('requester_id', 'Requester Id', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('status_dibutuhkan', 'Status Dibutuhkan', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('atasan_langsung', 'Atasan Langsung', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('alasan_permohonan', 'Alasan Permohonan', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('penempatan', 'Penempatan', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('usia', 'Usia', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('status_karyawan', 'Status Karyawan', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('marital_status', 'Marital Status', 'trim|required|max_length[10]');
		$this->form_validation->set_rules('pendidikan', 'Pendidikan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('pengalaman', 'Pengalaman', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('keterampilan', 'Keterampilan', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'requester_id' => $this->input->post('requester_id'),
				'jabatan' => $this->input->post('jabatan'),
				'jumlah' => $this->input->post('jumlah'),
				'status_dibutuhkan' => $this->input->post('status_dibutuhkan'),
				'atasan_langsung' => $this->input->post('atasan_langsung'),
				'alasan_permohonan' => $this->input->post('alasan_permohonan'),
				'penempatan' => $this->input->post('penempatan'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'usia' => $this->input->post('usia'),
				'status_karyawan' => $this->input->post('status_karyawan'),
				'marital_status' => $this->input->post('marital_status'),
				'pendidikan' => $this->input->post('pendidikan'),
				'pengalaman' => $this->input->post('pengalaman'),
				'keterampilan' => $this->input->post('keterampilan'),
			];

			
			$save_fptk = $this->model_fptk->change($id, $save_data);

			if ($save_fptk) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/fptk', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/fptk');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/fptk');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Fptks
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('fptk_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'fptk'), 'success');
        } else {
            set_message(cclang('error_delete', 'fptk'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Fptks
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('fptk_view');

		$this->data['fptk'] = $this->model_fptk->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('FPTK Detail');
		$this->render('backend/standard/administrator/fptk/fptk_view', $this->data);
	}
	
	/**
	* delete Fptks
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$fptk = $this->model_fptk->find($id);

		
		
		return $this->model_fptk->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('fptk_export');

		$this->model_fptk->export('fptk', 'fptk');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('fptk_export');

		$this->model_fptk->pdf('fptk', 'fptk');
	}
}


/* End of file fptk.php */
/* Location: ./application/controllers/administrator/Fptk.php */