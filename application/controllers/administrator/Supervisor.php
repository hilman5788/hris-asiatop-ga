<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Supervisor Controller
*| --------------------------------------------------------------------------
*| Supervisor site
*|
*/
class Supervisor extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_supervisor');
	}

	/**
	* show all Supervisors
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('supervisor_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['supervisors'] = $this->model_supervisor->get($filter, $field, $this->limit_page, $offset);
		$this->data['supervisor_counts'] = $this->model_supervisor->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/supervisor/index/',
			'total_rows'   => $this->model_supervisor->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Supervisor List');
		$this->render('backend/standard/administrator/supervisor/supervisor_list', $this->data);
	}
	
	/**
	* Add new supervisors
	*
	*/
	public function add()
	{
		$this->is_allowed('supervisor_add');

		$this->template->title('Supervisor New');
		$this->render('backend/standard/administrator/supervisor/supervisor_add', $this->data);
	}

	/**
	* Add New Supervisors
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('supervisor_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('supervisor_name', 'Supervisor Name', 'trim|required');
		$this->form_validation->set_rules('supervisor_grade', 'Grade Level', 'trim|required');
		$this->form_validation->set_rules('section_id', 'Section Name', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'supervisor_name' => $this->input->post('supervisor_name'),
				'supervisor_grade' => $this->input->post('supervisor_grade'),
				'supervisor_rank' => $this->input->post('supervisor_rank'),
				'supervisor_title' => $this->input->post('supervisor_title'),
				'supervisor_job_title' => $this->input->post('supervisor_job_title'),
				'section_id' => $this->input->post('section_id'),
			];

			
			$save_supervisor = $this->model_supervisor->store($save_data);

			if ($save_supervisor) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_supervisor;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/supervisor/edit/' . $save_supervisor, 'Edit Supervisor'),
						anchor('administrator/supervisor', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/supervisor/edit/' . $save_supervisor, 'Edit Supervisor')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/supervisor');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/supervisor');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Supervisors
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('supervisor_update');

		$this->data['supervisor'] = $this->model_supervisor->find($id);

		$this->template->title('Supervisor Update');
		$this->render('backend/standard/administrator/supervisor/supervisor_update', $this->data);
	}

	/**
	* Update Supervisors
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('supervisor_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('supervisor_name', 'Supervisor Name', 'trim|required');
		$this->form_validation->set_rules('supervisor_grade', 'Grade Level', 'trim|required');
		$this->form_validation->set_rules('section_id', 'Section Name', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'supervisor_name' => $this->input->post('supervisor_name'),
				'supervisor_grade' => $this->input->post('supervisor_grade'),
				'supervisor_rank' => $this->input->post('supervisor_rank'),
				'supervisor_title' => $this->input->post('supervisor_title'),
				'supervisor_job_title' => $this->input->post('supervisor_job_title'),
				'section_id' => $this->input->post('section_id'),
			];

			
			$save_supervisor = $this->model_supervisor->change($id, $save_data);

			if ($save_supervisor) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/supervisor', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/supervisor');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/supervisor');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Supervisors
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('supervisor_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'supervisor'), 'success');
        } else {
            set_message(cclang('error_delete', 'supervisor'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Supervisors
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('supervisor_view');

		$this->data['supervisor'] = $this->model_supervisor->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Supervisor Detail');
		$this->render('backend/standard/administrator/supervisor/supervisor_view', $this->data);
	}
	
	/**
	* delete Supervisors
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$supervisor = $this->model_supervisor->find($id);

		
		
		return $this->model_supervisor->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('supervisor_export');

		$this->model_supervisor->export('supervisor', 'supervisor');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('supervisor_export');

		$this->model_supervisor->pdf('supervisor', 'supervisor');
	}
}


/* End of file supervisor.php */
/* Location: ./application/controllers/administrator/Supervisor.php */