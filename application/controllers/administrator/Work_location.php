<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Work Location Controller
*| --------------------------------------------------------------------------
*| Work Location site
*|
*/
class Work_location extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_work_location');
	}

	/**
	* show all Work Locations
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('work_location_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['work_locations'] = $this->model_work_location->get($filter, $field, $this->limit_page, $offset);
		$this->data['work_location_counts'] = $this->model_work_location->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/work_location/index/',
			'total_rows'   => $this->model_work_location->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Work Location List');
		$this->render('backend/standard/administrator/work_location/work_location_list', $this->data);
	}
	
	/**
	* Add new work_locations
	*
	*/
	public function add()
	{
		$this->is_allowed('work_location_add');

		$this->template->title('Work Location New');
		$this->render('backend/standard/administrator/work_location/work_location_add', $this->data);
	}

	/**
	* Add New Work Locations
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('work_location_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('work_location_name', 'Work Location Name', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'work_location_name' => $this->input->post('work_location_name'),
				'work_location_desc' => $this->input->post('work_location_desc'),
			];

			
			$save_work_location = $this->model_work_location->store($save_data);

			if ($save_work_location) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_work_location;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/work_location/edit/' . $save_work_location, 'Edit Work Location'),
						anchor('administrator/work_location', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/work_location/edit/' . $save_work_location, 'Edit Work Location')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/work_location');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/work_location');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Work Locations
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('work_location_update');

		$this->data['work_location'] = $this->model_work_location->find($id);

		$this->template->title('Work Location Update');
		$this->render('backend/standard/administrator/work_location/work_location_update', $this->data);
	}

	/**
	* Update Work Locations
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('work_location_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('work_location_name', 'Work Location Name', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'work_location_name' => $this->input->post('work_location_name'),
				'work_location_desc' => $this->input->post('work_location_desc'),
			];

			
			$save_work_location = $this->model_work_location->change($id, $save_data);

			if ($save_work_location) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/work_location', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/work_location');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/work_location');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Work Locations
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('work_location_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'work_location'), 'success');
        } else {
            set_message(cclang('error_delete', 'work_location'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Work Locations
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('work_location_view');

		$this->data['work_location'] = $this->model_work_location->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Work Location Detail');
		$this->render('backend/standard/administrator/work_location/work_location_view', $this->data);
	}
	
	/**
	* delete Work Locations
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$work_location = $this->model_work_location->find($id);

		
		
		return $this->model_work_location->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('work_location_export');

		$this->model_work_location->export('work_location', 'work_location');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('work_location_export');

		$this->model_work_location->pdf('work_location', 'work_location');
	}
}


/* End of file work_location.php */
/* Location: ./application/controllers/administrator/Work Location.php */