<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Suplies Distribution Controller
*| --------------------------------------------------------------------------
*| Suplies Distribution site
*|
*/
class Suplies_distribution extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_suplies_distribution');
	}

	/**
	* show all Suplies Distributions
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('suplies_distribution_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['suplies_distributions'] = $this->model_suplies_distribution->get($filter, $field, $this->limit_page, $offset);
		$this->data['suplies_distribution_counts'] = $this->model_suplies_distribution->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/suplies_distribution/index/',
			'total_rows'   => $this->model_suplies_distribution->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Suplies Distribution List');
		$this->render('backend/standard/administrator/suplies_distribution/suplies_distribution_list', $this->data);
	}
	
	/**
	* Add new suplies_distributions
	*
	*/
	public function add()
	{
		$this->is_allowed('suplies_distribution_add');

		$this->template->title('Suplies Distribution New');
		$this->render('backend/standard/administrator/suplies_distribution/suplies_distribution_add', $this->data);
	}

	/**
	* Add New Suplies Distributions
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('suplies_distribution_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('supliesdistribution_name', 'Supliesdistribution Name', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('zip', 'Zip', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('paid', 'Paid', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'supliesdistribution_name' => $this->input->post('supliesdistribution_name'),
				'description' => $this->input->post('description'),
				'address' => $this->input->post('address'),
				'city' => $this->input->post('city'),
				'date' => $this->input->post('date'),
				'phone' => $this->input->post('phone'),
				'zip' => $this->input->post('zip'),
				'paid' => $this->input->post('paid'),
			];

			
			$save_suplies_distribution = $this->model_suplies_distribution->store($save_data);

			if ($save_suplies_distribution) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_suplies_distribution;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/suplies_distribution/edit/' . $save_suplies_distribution, 'Edit Suplies Distribution'),
						anchor('administrator/suplies_distribution', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/suplies_distribution/edit/' . $save_suplies_distribution, 'Edit Suplies Distribution')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/suplies_distribution');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/suplies_distribution');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Suplies Distributions
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('suplies_distribution_update');

		$this->data['suplies_distribution'] = $this->model_suplies_distribution->find($id);

		$this->template->title('Suplies Distribution Update');
		$this->render('backend/standard/administrator/suplies_distribution/suplies_distribution_update', $this->data);
	}

	/**
	* Update Suplies Distributions
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('suplies_distribution_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('supliesdistribution_name', 'Supliesdistribution Name', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('city', 'City', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('zip', 'Zip', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('paid', 'Paid', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'supliesdistribution_name' => $this->input->post('supliesdistribution_name'),
				'description' => $this->input->post('description'),
				'address' => $this->input->post('address'),
				'city' => $this->input->post('city'),
				'date' => $this->input->post('date'),
				'phone' => $this->input->post('phone'),
				'zip' => $this->input->post('zip'),
				'paid' => $this->input->post('paid'),
			];

			
			$save_suplies_distribution = $this->model_suplies_distribution->change($id, $save_data);

			if ($save_suplies_distribution) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/suplies_distribution', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/suplies_distribution');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/suplies_distribution');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Suplies Distributions
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('suplies_distribution_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'suplies_distribution'), 'success');
        } else {
            set_message(cclang('error_delete', 'suplies_distribution'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Suplies Distributions
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('suplies_distribution_view');

		$this->data['suplies_distribution'] = $this->model_suplies_distribution->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Suplies Distribution Detail');
		$this->render('backend/standard/administrator/suplies_distribution/suplies_distribution_view', $this->data);
	}
	
	/**
	* delete Suplies Distributions
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$suplies_distribution = $this->model_suplies_distribution->find($id);

		
		
		return $this->model_suplies_distribution->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('suplies_distribution_export');

		$this->model_suplies_distribution->export('suplies_distribution', 'suplies_distribution');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('suplies_distribution_export');

		$this->model_suplies_distribution->pdf('suplies_distribution', 'suplies_distribution');
	}
}


/* End of file suplies_distribution.php */
/* Location: ./application/controllers/administrator/Suplies Distribution.php */