<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Company Controller
*| --------------------------------------------------------------------------
*| Company site
*|
*/
class Company extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_company');
	}

	/**
	* show all Companys
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('company_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['companys'] = $this->model_company->get($filter, $field, $this->limit_page, $offset);
		$this->data['company_counts'] = $this->model_company->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/company/index/',
			'total_rows'   => $this->model_company->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Company List');
		$this->render('backend/standard/administrator/company/company_list', $this->data);
	}
	
	/**
	* Add new companys
	*
	*/
	public function add()
	{
		$this->is_allowed('company_add');

		$this->template->title('Company New');
		$this->render('backend/standard/administrator/company/company_add', $this->data);
	}

	/**
	* Add New Companys
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('company_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
		$this->form_validation->set_rules('company_company_logo_name', 'Logo', 'trim');
		

		if ($this->form_validation->run()) {
			$company_company_logo_uuid = $this->input->post('company_company_logo_uuid');
			$company_company_logo_name = $this->input->post('company_company_logo_name');
		
			$save_data = [
				'company_name' => $this->input->post('company_name'),
				'company_start_date' => $this->input->post('company_start_date'),
				'company_vision' => $this->input->post('company_vision'),
				'company_mission' => $this->input->post('company_mission'),
				'company_contact_email' => $this->input->post('company_contact_email'),
				'company_contact_phone' => $this->input->post('company_contact_phone'),
				'company_contact_fax' => $this->input->post('company_contact_fax'),
				'company_type' => $this->input->post('company_type'),
				'company_business_type' => $this->input->post('company_business_type'),
				'company_address_location' => $this->input->post('company_address_location'),
				'company_address_city' => $this->input->post('company_address_city'),
				'company_address_zip_code' => $this->input->post('company_address_zip_code'),
				'company_address_country' => $this->input->post('company_address_country'),
				'company_tax_npkp' => $this->input->post('company_tax_npkp'),
				'company_tax_npwp' => $this->input->post('company_tax_npwp'),
				'company_tax_location' => $this->input->post('company_tax_location'),
				'company_bank_name' => $this->input->post('company_bank_name'),
				'company_bank_account_no' => $this->input->post('company_bank_account_no'),
				'company_bank_account_name' => $this->input->post('company_bank_account_name'),
				'company_is_holding' => $this->input->post('company_is_holding'),
			];

			if (!is_dir(FCPATH . '/uploads/company/')) {
				mkdir(FCPATH . '/uploads/company/');
			}

			if (!empty($company_company_logo_name)) {
				$company_company_logo_name_copy = date('YmdHis') . '-' . $company_company_logo_name;

				rename(FCPATH . 'uploads/tmp/' . $company_company_logo_uuid . '/' . $company_company_logo_name, 
						FCPATH . 'uploads/company/' . $company_company_logo_name_copy);

				if (!is_file(FCPATH . '/uploads/company/' . $company_company_logo_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['company_logo'] = $company_company_logo_name_copy;
			}
		
			
			$save_company = $this->model_company->store($save_data);

			if ($save_company) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_company;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/company/edit/' . $save_company, 'Edit Company'),
						anchor('administrator/company', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/company/edit/' . $save_company, 'Edit Company')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/company');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/company');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Companys
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('company_update');

		$this->data['company'] = $this->model_company->find($id);

		$this->template->title('Company Update');
		$this->render('backend/standard/administrator/company/company_update', $this->data);
	}

	/**
	* Update Companys
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('company_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
		$this->form_validation->set_rules('company_company_logo_name', 'Logo', 'trim');
		
		if ($this->form_validation->run()) {
			$company_company_logo_uuid = $this->input->post('company_company_logo_uuid');
			$company_company_logo_name = $this->input->post('company_company_logo_name');
		
			$save_data = [
				'company_name' => $this->input->post('company_name'),
				'company_start_date' => $this->input->post('company_start_date'),
				'company_vision' => $this->input->post('company_vision'),
				'company_mission' => $this->input->post('company_mission'),
				'company_contact_email' => $this->input->post('company_contact_email'),
				'company_contact_phone' => $this->input->post('company_contact_phone'),
				'company_contact_fax' => $this->input->post('company_contact_fax'),
				'company_type' => $this->input->post('company_type'),
				'company_business_type' => $this->input->post('company_business_type'),
				'company_address_location' => $this->input->post('company_address_location'),
				'company_address_city' => $this->input->post('company_address_city'),
				'company_address_zip_code' => $this->input->post('company_address_zip_code'),
				'company_address_country' => $this->input->post('company_address_country'),
				'company_tax_npkp' => $this->input->post('company_tax_npkp'),
				'company_tax_npwp' => $this->input->post('company_tax_npwp'),
				'company_tax_location' => $this->input->post('company_tax_location'),
				'company_bank_name' => $this->input->post('company_bank_name'),
				'company_bank_account_no' => $this->input->post('company_bank_account_no'),
				'company_bank_account_name' => $this->input->post('company_bank_account_name'),
				'company_is_holding' => $this->input->post('company_is_holding'),
			];

			if (!is_dir(FCPATH . '/uploads/company/')) {
				mkdir(FCPATH . '/uploads/company/');
			}

			if (!empty($company_company_logo_uuid)) {
				$company_company_logo_name_copy = date('YmdHis') . '-' . $company_company_logo_name;

				rename(FCPATH . 'uploads/tmp/' . $company_company_logo_uuid . '/' . $company_company_logo_name, 
						FCPATH . 'uploads/company/' . $company_company_logo_name_copy);

				if (!is_file(FCPATH . '/uploads/company/' . $company_company_logo_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['company_logo'] = $company_company_logo_name_copy;
			}
		
			
			$save_company = $this->model_company->change($id, $save_data);

			if ($save_company) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/company', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/company');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/company');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Companys
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('company_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'company'), 'success');
        } else {
            set_message(cclang('error_delete', 'company'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Companys
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('company_view');

		$this->data['company'] = $this->model_company->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Company Detail');
		$this->render('backend/standard/administrator/company/company_view', $this->data);
	}
	
	/**
	* delete Companys
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$company = $this->model_company->find($id);

		if (!empty($company->company_logo)) {
			$path = FCPATH . '/uploads/company/' . $company->company_logo;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_company->remove($id);
	}
	
	/**
	* Upload Image Company	* 
	* @return JSON
	*/
	public function upload_company_logo_file()
	{
		if (!$this->is_allowed('company_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'company',
			'allowed_types' => 'jpg',
		]);
	}

	/**
	* Delete Image Company	* 
	* @return JSON
	*/
	public function delete_company_logo_file($uuid)
	{
		if (!$this->is_allowed('company_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'company_logo', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'company',
            'primary_key'       => 'company_id',
            'upload_path'       => 'uploads/company/'
        ]);
	}

	/**
	* Get Image Company	* 
	* @return JSON
	*/
	public function get_company_logo_file($id)
	{
		if (!$this->is_allowed('company_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$company = $this->model_company->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'company_logo', 
            'table_name'        => 'company',
            'primary_key'       => 'company_id',
            'upload_path'       => 'uploads/company/',
            'delete_endpoint'   => 'administrator/company/delete_company_logo_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('company_export');

		$this->model_company->export('company', 'company');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('company_export');

		$this->model_company->pdf('company', 'company');
	}
}


/* End of file company.php */
/* Location: ./application/controllers/administrator/Company.php */