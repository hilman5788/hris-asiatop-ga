<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Facility Request Controller
*| --------------------------------------------------------------------------
*| Facility Request site
*|
*/
class Facility_request extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_facility_request');
	}

	/**
	* show all Facility Requests
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('facility_request_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['facility_requests'] = $this->model_facility_request->get($filter, $field, $this->limit_page, $offset);
		$this->data['facility_request_counts'] = $this->model_facility_request->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/facility_request/index/',
			'total_rows'   => $this->model_facility_request->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Facility Request List');
		$this->render('backend/standard/administrator/facility_request/facility_request_list', $this->data);
	}
	
	/**
	* Add new facility_requests
	*
	*/
	public function add()
	{
		$this->is_allowed('facility_request_add');

		$this->template->title('Facility Request New');
		$this->render('backend/standard/administrator/facility_request/facility_request_add', $this->data);
	}

	/**
	* Add New Facility Requests
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('facility_request_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('facility_request_name', 'Facility Request Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_name', 'Goods Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_category', 'Goods Category', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'facility_request_name' => $this->input->post('facility_request_name'),
				'description' => $this->input->post('description'),
				'goods_name' => $this->input->post('goods_name'),
				'goods_category' => $this->input->post('goods_category'),
				'total' => $this->input->post('total'),
				'date' => $this->input->post('date'),
				'price' => $this->input->post('price'),
			];

			
			$save_facility_request = $this->model_facility_request->store($save_data);

			if ($save_facility_request) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_facility_request;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/facility_request/edit/' . $save_facility_request, 'Edit Facility Request'),
						anchor('administrator/facility_request', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/facility_request/edit/' . $save_facility_request, 'Edit Facility Request')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/facility_request');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/facility_request');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Facility Requests
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('facility_request_update');

		$this->data['facility_request'] = $this->model_facility_request->find($id);

		$this->template->title('Facility Request Update');
		$this->render('backend/standard/administrator/facility_request/facility_request_update', $this->data);
	}

	/**
	* Update Facility Requests
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('facility_request_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('facility_request_name', 'Facility Request Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_name', 'Goods Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_category', 'Goods Category', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'facility_request_name' => $this->input->post('facility_request_name'),
				'description' => $this->input->post('description'),
				'goods_name' => $this->input->post('goods_name'),
				'goods_category' => $this->input->post('goods_category'),
				'total' => $this->input->post('total'),
				'date' => $this->input->post('date'),
				'price' => $this->input->post('price'),
			];

			
			$save_facility_request = $this->model_facility_request->change($id, $save_data);

			if ($save_facility_request) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/facility_request', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/facility_request');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/facility_request');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Facility Requests
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('facility_request_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'facility_request'), 'success');
        } else {
            set_message(cclang('error_delete', 'facility_request'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Facility Requests
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('facility_request_view');

		$this->data['facility_request'] = $this->model_facility_request->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Facility Request Detail');
		$this->render('backend/standard/administrator/facility_request/facility_request_view', $this->data);
	}
	
	/**
	* delete Facility Requests
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$facility_request = $this->model_facility_request->find($id);

		
		
		return $this->model_facility_request->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('facility_request_export');

		$this->model_facility_request->export('facility_request', 'facility_request');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('facility_request_export');

		$this->model_facility_request->pdf('facility_request', 'facility_request');
	}
}


/* End of file facility_request.php */
/* Location: ./application/controllers/administrator/Facility Request.php */