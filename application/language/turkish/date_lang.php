<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['date_year']      = 'Yıl';
$lang['date_years']     = 'Yıllar';
$lang['date_month']     = 'Ay';
$lang['date_months']    = 'Aylar';
$lang['date_week']      = 'Hafta';
$lang['date_weeks']     = 'Haftalar';
$lang['date_day']       = 'Gün';
$lang['date_days']      = 'Günler';
$lang['date_hour']      = 'Saat';
$lang['date_hours']     = 'Saatler';
$lang['date_minute']    = 'Dakika';
$lang['date_minutes']   = 'Dakikalar';
$lang['date_second']    = 'Saniye';
$lang['date_seconds']   = 'Saniyeler';

$lang['UM12']   = '(UTC -12:00) Baker/Howland Adası';
$lang['UM11']   = '(UTC -11:00) Niue';
$lang['UM10']   = '(UTC -10:00) Hawaii-Aleutian standard Saati, Cook Adaları, Tahiti';
$lang['UM95']   = '(UTC -9:30) Marquesas Adaları';
$lang['UM9']    = '(UTC -9:00) Alaska standard Saati, Gambier Adaları';
$lang['UM8']    = '(UTC -8:00) Pacific standard Saati, Clipperton Adası';
$lang['UM7']    = '(UTC -7:00) Mountain standard Saati';
$lang['UM6']    = '(UTC -6:00) Merkezi standard Saati';
$lang['UM5']    = '(UTC -5:00) Doğu standard Saati, Batı Caribbean standard Saati';
$lang['UM45']   = '(UTC -4:30) Venezuela standard Saati';
$lang['UM4']    = '(UTC -4:00) Atlantic standard Saati, Doğu Caribbean standard Saati';
$lang['UM35']   = '(UTC -3:30) Newfoundland standard Saati';
$lang['UM3']    = '(UTC -3:00) Arjantin, Brezilya, Fransız Guanası, Uruguay';
$lang['UM2']    = '(UTC -2:00) Güney Georgia/Güney Sandwich Adaları';
$lang['UM1']    = '(UTC -1:00) Azores, Cape Verde Adaları';
$lang['UTC']    = '(UTC) Greenwich Ortalama Saati, Batı Avrupa Saati';
$lang['UP1']    = '(UTC +1:00) Orta Avrupa Saati, Batı Africa Saati';
$lang['UP2']    = '(UTC +2:00) Orta Africa Saati, Doğu Avrupa Saati, Kaliningrad Saati';
$lang['UP3']    = '(UTC +3:00) Moskova Saati, Doğu Africa Saati, Arabia standard Saati';
$lang['UP35']   = '(UTC +3:30) İran standard Saati';
$lang['UP4']    = '(UTC +4:00) Azerbaycan standard Saati, Samara Saati';
$lang['UP45']   = '(UTC +4:30) Afganistan';
$lang['UP5']    = '(UTC +5:00) Pakistan standard Saati, Yekaterinburg Saati';
$lang['UP55']   = '(UTC +5:30) Hindistan standard Saati, Sri Lanka Saati';
$lang['UP575']  = '(UTC +5:45) Nepal Saati';
$lang['UP6']    = '(UTC +6:00) Bangladeş standard Saati, Bhutan Saati, Omsk Saati';
$lang['UP65']   = '(UTC +6:30) Cocos Adaları, Myanmar';
$lang['UP7']    = '(UTC +7:00) Krasnoyarsk Saati, Cambodia, Laos, Thailand, Vietnam';
$lang['UP8']    = '(UTC +8:00) Avustralya Batı standard Saati, Beijing Saati, Irkutsk Saati';
$lang['UP875']  = '(UTC +8:45) Avustralya Orta Batı standard Saati';
$lang['UP9']    = '(UTC +9:00) Japonya standard Saati, Kore standard Saati, Yakutsk Saati';
$lang['UP95']   = '(UTC +9:30) Avustralya Merkezi standard Saati';
$lang['UP10']   = '(UTC +10:00) Avustralya Doğu standard Saati, Vladivostok Saati';
$lang['UP105']  = '(UTC +10:30) Lord Howe Adası';
$lang['UP11']   = '(UTC +11:00) Srednekolymsk Saati, Solomon Adaları, Vanuatu';
$lang['UP115']  = '(UTC +11:30) Norfolk Adası';
$lang['UP12']   = '(UTC +12:00) Fiji, Gilbert Adaları, Kamchatka Saati, Yeni Zelanda standard Saati';
$lang['UP1275'] = '(UTC +12:45) Chatham Adaları standard Saati';
$lang['UP13']   = '(UTC +13:00) Samoa Saati Bölgesi, Phoenix Adaları Saati, Tonga';
$lang['UP14']   = '(UTC +14:00) Line Adaları';
