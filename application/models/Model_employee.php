<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_employee extends MY_Model {

	private $primary_key 	= 'employee_id';
	private $table_name 	= 'employee';
	private $field_search 	= ['employee_nik', 'employee_name', 'employee_hire_date', 'employee_grade_id', 'employee_department_id', 'employee_position_id', 'employee_work_location_id', 'employee_birth_date'];

	public function __construct()
	{
		$config = array(
			'primary_key' 	=> $this->primary_key,
		 	'table_name' 	=> $this->table_name,
		 	'field_search' 	=> $this->field_search,
		 );

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "employee.".$field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . "employee.".$field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . "employee.".$field . " LIKE '%" . $q . "%' )";
        }

		$this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "employee.".$field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . "employee.".$field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . "employee.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
        	$this->db->select($select_field);
        }
		
		$this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        $this->db->order_by('employee.'.$this->primary_key, "DESC");
		$query = $this->db->get($this->table_name);

		return $query->result();
	}

    public function join_avaiable() {
        $this->db->join('department', 'department.department_id = employee.employee_department_id', 'LEFT');
        $this->db->join('position', 'position.position_id = employee.employee_position_id', 'LEFT');
        $this->db->join('work_location', 'work_location.work_location_id = employee.employee_work_location_id', 'LEFT');
        
        return $this;
    }

    public function filter_avaiable() {
        
        return $this;
    }

}

/* End of file Model_employee.php */
/* Location: ./application/models/Model_employee.php */