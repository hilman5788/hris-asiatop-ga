<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_tindakan_preservasi_perawatan_bp extends MY_Model {

	private $primary_key 	= 'id';
	private $table_name 	= 'tindakan_preservasi_perawatan_bp';
	private $field_search 	= ['header_tanggal', 'deskripsi_dokument', 'id_survey_kondisi', 'jenis_tindakan_preservasi', 'jenis_koleksi', 'jumlah_halaman', 'unit_kerja', 'intitusi_penanggungjawab', 'foto_dokumen'];

	public function __construct()
	{
		$config = array(
			'primary_key' 	=> $this->primary_key,
		 	'table_name' 	=> $this->table_name,
		 	'field_search' 	=> $this->field_search,
		 );

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "tindakan_preservasi_perawatan_bp.".$field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . "tindakan_preservasi_perawatan_bp.".$field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . "tindakan_preservasi_perawatan_bp.".$field . " LIKE '%" . $q . "%' )";
        }

		$this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "tindakan_preservasi_perawatan_bp.".$field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . "tindakan_preservasi_perawatan_bp.".$field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . "tindakan_preservasi_perawatan_bp.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
        	$this->db->select($select_field);
        }
		
		$this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        $this->db->order_by('tindakan_preservasi_perawatan_bp.'.$this->primary_key, "DESC");
		$query = $this->db->get($this->table_name);

		return $query->result();
	}

    public function join_avaiable() {
        $this->db->join('survey_kondisi', 'survey_kondisi.id = tindakan_preservasi_perawatan_bp.id_survey_kondisi', 'LEFT');
        
        return $this;
    }

    public function filter_avaiable() {
        $this->db->where('header_petugas', get_user_data('id'));
        
        return $this;
    }

}

/* End of file Model_tindakan_preservasi_perawatan_bp.php */
/* Location: ./application/models/Model_tindakan_preservasi_perawatan_bp.php */